﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService1
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "RestCalculatorService" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez RestCalculatorService.svc ou RestCalculatorService.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class RestCalculatorService : IRestCalculatorService
    {
        public double Add(string n1, string n2)
        {
            double result = Convert.ToDouble(n1) + Convert.ToDouble(n2);
            Console.WriteLine("Received Add({0},{1})", n1, n2);
            // Code added to write output to the console window.  
            Console.WriteLine("Return: {0}", result);
            return result;
        }

        public double Subtract(string n1, string n2)
        {
            double result = Convert.ToDouble(n1) - Convert.ToDouble(n2);
            Console.WriteLine("Received Subtract({0},{1})", n1, n2);
            Console.WriteLine("Return: {0}", result);
            return result;
        }

        public double Multiply(string n1, string n2)
        {
            double result = Convert.ToDouble(n1) * Convert.ToDouble(n2);
            Console.WriteLine("Received Multiply({0},{1})", n1, n2);
            Console.WriteLine("Return: {0}", result);
            return result;
        }

        public double Divide(string n1, string n2)
        {
            double result = Convert.ToDouble(n1) / Convert.ToDouble(n2);
            Console.WriteLine("Received Divide({0},{1})", n1, n2);
            Console.WriteLine("Return: {0}", result);
            return result;
        }
    }
}
