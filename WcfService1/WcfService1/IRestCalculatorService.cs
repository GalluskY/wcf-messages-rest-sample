﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IRestCalculatorService" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IRestCalculatorService
    {
        [OperationContract]
        [WebGet(UriTemplate = "add/{n1}/{n2}", ResponseFormat = WebMessageFormat.Json)]
        double Add(string n1, string n2);
        [OperationContract]
        [WebGet(UriTemplate = "subtract/{n1}/{n2}", ResponseFormat = WebMessageFormat.Json)]
        double Subtract(string n1, string n2);
        [OperationContract]
        [WebGet(UriTemplate = "multiply/{n1}/{n2}", ResponseFormat = WebMessageFormat.Json)]
        double Multiply(string n1, string n2);
        [OperationContract]
        [WebGet(UriTemplate = "divide/{n1}/{n2}", ResponseFormat = WebMessageFormat.Json)]
        double Divide(string n1, string n2);
    }
}
