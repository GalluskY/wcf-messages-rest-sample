﻿using System;
using WcfService1;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;

namespace WcfService1Host
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseURI = "http://localhost:50000/";
            var wcfEP = "wcf";
            var restEP = "rest";

            // Step 1 Create a URI to serve as the base address.  
            Uri baseAddress = new Uri(baseURI);

            // Step 2.1 Create a ServiceHost instance for WCF services
            ServiceHost wcfHost = new ServiceHost(typeof(CalculatorService), baseAddress);
            // Step 2.2 Create a WebServiceHost instance for REST services
            WebServiceHost restHost = new WebServiceHost(typeof(RestCalculatorService), baseAddress);

            try
            {
                // Step 3 Add a service endpoint for the WCF service  
                wcfHost.AddServiceEndpoint(typeof(ICalculator), new WSHttpBinding(), wcfEP);
                // Step 3.2 Add a service endpoint for the REST service
                restHost.AddServiceEndpoint(typeof(IRestCalculatorService), new WebHttpBinding(), restEP);
                // Step 4 Enable metadata exchange.  
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                wcfHost.Description.Behaviors.Add(smb);
                
                // Step 5 Start the services
                wcfHost.Open();
                restHost.Open();

                // Display some useful information
                Console.WriteLine("The service is ready.");
                Console.WriteLine(string.Format("The EP for the rest calculator service is : {0}{1}", baseURI, restEP));
                Console.WriteLine(string.Format("The EP for the calculator service is : {0}{1}", baseURI, wcfEP));
                Console.WriteLine("Press <ENTER> to terminate service.");
                Console.WriteLine();
                Console.ReadLine();

                // Close the services to shutdown
                wcfHost.Close();
                restHost.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                Console.Read();
                wcfHost.Abort();
                restHost.Abort();
            }
        }
    }
}
